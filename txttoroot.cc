#include <TH1D.h>
#include <TStyle.h>
#include <TString.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>
#include <TGraph.h>

//C, C++
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <sstream>

using namespace std;

void convtoroot(TString txtFile,
			   TString rootFile, Int_t NP);


int main(int argc, char *argv[]){

  if(argc==3){

    TString txtFile = argv[1];
    TString rootFile = txtFile + ".root";
    Int_t Npoints = atoi(argv[2]);
    cout<<"Npoints : "<<Npoints<<endl;

    convtoroot(txtFile,
	       rootFile, Npoints);

  }
  else{
    cout<<"  ERROR ---> in input arguments "<<endl
	<<"             [1] : name of a ASCI file containing waveforms"<<endl
	<<"             [2] : number of points in waveforms"<<endl;
  }

  return 0;
}
void convtoroot(TString txtFile,
		 TString rootFile, Int_t NP){

  Int_t i = 0;


  ///////////////////Root file with data/////////////////
  TFile *hfile = new TFile( rootFile, "RECREATE", "CRT Data processed by convertCRT2root", 1);
  if (hfile->IsZombie()) {
    cout << "PROBLEM with the initialization of the output ROOT ntuple "
	 << rootFile << ": check that the path is correct!!!"
	 << endl;
    exit(-1);
  }
  TTree *tree = new TTree("T", "CRT Data Tree");
  tree->SetAutoSave(1000000);
  TTree::SetBranchStyle(0);

  Int_t NPoints=500000;
  Double_t *V = new Double_t[NPoints];
  Double_t *x = new Double_t[NPoints];
  Double_t dt = 0.0, Vbias = 0.0, Ibias = 0.0, IDiode = 0.0, IDiodeStd = 0.0;

  Int_t WfNumber=0;

  tree-> Branch("NPoints",&NPoints,"NPoints/I");
  tree-> Branch("Volt",V,"V[NPoints]/D");
  tree-> Branch("dt",&dt,"dt/D");
  tree-> Branch("Vbias",&Vbias,"Vbias/D");
  tree-> Branch("Ibias",&Ibias,"Ibias/D");
	tree-> Branch("IDiode",&IDiode,"IDiode/D");
  tree-> Branch("IDiodeStd",&IDiodeStd,"IDiodeStd/D");
//  tree->Branch("Time_ns", time, "time[10000]/I");
 ///////////////////////////////////////////////////////

//  time[0]=StartTime;
//  for(i = 1;i<10000;i++) time[i]=time[i-1]+50;

  TString ch1, ch2;
  bool find = true;
  TString outTXTFileName = "New_"+txtFile;
	//ofstream DataOut(outTXTFileName, ios::app);

  ifstream indata;
  indata.open(txtFile.Data());
  assert(indata.is_open());
  Int_t WfNumberPrev = 0, point=0, NPMean;
	Double_t MaxWaveforms = 100000;
	Double_t Ratio = 1.;

  TH1D *AmpHist = new TH1D("AmpHist", "", 1000., -0.1, 0.1);
  TH1D *CurrentHist;

  while((!indata.eof())&&(WfNumber<MaxWaveforms)){
     indata>>ch1;
     if(ch1 == "waveform"){
        WfNumberPrev = WfNumber;
        indata>>WfNumber;
        if(WfNumber%100 == 0) cout<<"WfNumber : "<<WfNumber<<endl;
     }
     else if(ch1 == "dt"){
        indata>>dt;
        if(WfNumber%100 == 0) cout<<"dt : "<<dt<<endl;
     }
     else if(ch1 == "Npoints"){
        indata>>NPoints;
				NPoints = NPoints/Ratio;
        if(WfNumber == 0) NPMean = NPoints;
        if(WfNumber%100 == 0) cout<<"NPoints : "<<NPoints<<endl;
        indata>>ch1;
        indata>>Vbias;
        if(WfNumber%100 == 0) cout<<"Vbias : "<<Vbias<<endl;
        indata>>ch1;
        indata>>Ibias;
        if(WfNumber%100 == 0) cout<<"Ibias : "<<Ibias<<endl;
				if(WfNumber == 0) CurrentHist = new TH1D("CurrentHist", "", 10000., 0.2*Ibias, 1.5*Ibias);
				CurrentHist->Fill(Ibias);
				indata>>ch1;
				indata>>ch1;
				indata>>IDiode;
				if(WfNumber%100 == 0) cout<<"IDiode : "<<IDiode<<endl;
				indata>>ch1;
				indata>>ch1;
				indata>>ch1;
				indata>>IDiodeStd;
				if(WfNumber%100 == 0) cout<<"IDiodeStd : "<<IDiodeStd<<endl;
        point = 0;
				//cout<<"NP : "<<NP<<" NPoints : "<<NPoints<<endl;
        if(NPoints == NP){
					//cout<<"true"<<endl;
           for(i=0; i<NPoints; i++){
              indata>>ch1;
              V[i] = -1.0*atof(ch1); //- 3.21316e-02;
							x[i] = i*dt;
              AmpHist->Fill(V[i]);
           }
					 //cout<<endl;
					 //DataOut<<endl;
        }
        if(NPoints == NP) tree->Fill();
				//indata>>ch1;
				//cout<<ch1<<endl;
				/*TCanvas *c1 = new TCanvas("c1", "waveform 1", 800, 800);
				TGraph *gr = new TGraph(NP, x, V);
				gr->Draw("AP");
				c1->SaveAs("waveform1.root");*/
     }
     else {
        indata>>ch1;
				//cout<<ch1<<endl;
     }
  }

  indata.close();
  hfile = tree->GetCurrentFile();
  hfile->Write();
  hfile->Close();

}
